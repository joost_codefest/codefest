<?php
session_start();
session_destroy();
session_start();
//var_dump($_POST);
date_default_timezone_set('Europe/Amsterdam');
require_once("modules/required/security.php");
require_once("./auth/dbh_auth.php");

$sec = new Security();
$dbh = new ADbh();

$pass = $sec->HashPassword($_POST['password']);
$check = $dbh->Login($_POST['email'], $pass);
if($check)
{
    $_SESSION['ID'] = $dbh->GetIDByEmail($_POST['email']);
    echo 1;
}
else
{
    echo 0;
}
?>
