<?php
session_start();
if(empty($_SESSION['ID']))
{
    echo 1;
    die;
}
require_once("modules/required/security.php");
require_once("modules/required/dbh.php");
require_once("ulevel.php");
require_once("config/main.php");

$type = $_POST['type'];
$data = $_POST['data'];
$sec = new Security();
$dbh = new Dbh();
$ul = new ULevel();
$conf = new Config();
$check = $ul->CheckFunctionLevel($dbh, $conf->FunctionLevels, $type);
var_dump($check);
if($check == false)
{
    echo 0;
}

switch($type)
{
    case 'request_freedays':
        $dbh->RequestPermittance($data['start_date'], $data['end_date'], $data['request_type'], $data['approve']);
    break;
    case 'register_bhds':
        $dbh->AddHoliday($_POST['holiday'], $_POST['start_date'], $_POST['end_date']);
    break;
    case 'reset_password':
        $c = $dbh->EmailExists($_POST['email']);
        if($c)
        {
            if($dbh->AccountActivated($_POST['email']))
            {
                if(!$dbh->PassResetExists($_POST['email']))
                {
                    $hash = $sec->GetAcHash();
                    $date_time = date('Y-m-d H:i:s');
                    $check = $dbh->SetPassRecovery($_POST['email'], $hash, $date_time);
                    if($check)
                    {
                        echo 1;
                        $email->ResetPassword($_POST['email'], $hash);
                    }
                    else
                    {
                        echo 0;
                    }
                }
                else
                {
                    echo 0;
                }
            }
            else
            {
                echo 0;
            }
        }
        else
        {
            echo 0;
        }
    break;
}
?>
