<?php
class Controller extends Page
{
    public function RenderFinal($data)
    {?>
        <?$this->html->RegisterCores();?>
        <?$this->html->SetStyle('styles')?>
        <?$this->html->SetScript('bootstrap.min')?>
        <?$this->html->RegisterHead();?>
        <?$this->html->GetFirstBody();?>
        <div class="container-fluid">
            <div class="row">
                <div class="content col-md-12">
                    <ul class="nav nav-pills-stacked">
                        <li role="presentation" class="active"><a href="index.php">terug</a></li>
                        <li role="presentation" class="active"><a href="index.php?page=regemployee">Registreer nieuwe medewerker</a></li>
                        <li role="presentation" class="active"><a href="index.php?page=holidays">Verplichte vakantiedagen</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <h2>Adminsitratie</h2>
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Deeltijdfactor</td>
                        <td><input type="text" name="parttime_factor" placeholder="in te vullen door medewerker" /></td>
                    </tr>
                    <tr>
                        <td>Aantal dagen voor invoer vrije dag</td>
                        <td><input type="text" name="number_of_days" placeholder="in te vullen door medewerker" /></td>
                    </tr>
                    <tr>
                        <td>Deeltijdfactor vakantie dagen</td>
                        <td><input type="text" name="factor_holidays" placeholder="in te vullen door medewerker" /></td>
                    </tr>
                    <tr>
                        <td>Prijs inkoop vrije dag</td>
                        <td><input type="text" name="price_dayoff" placeholder="in te vullen door medewerker" /></td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>afdeling</th>
                        <th>huide bezetting</th>
                        <th>minimum bezetting</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php echo $this->dbh->GetDepartmentView(); ?>
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary">Doorsturen</button>
            </div>
        </div>
        <?$this->html->GetLastBody();?>
    <?}
}
?>