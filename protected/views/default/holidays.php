<?php
class Controller extends Page
{
    public function RenderFinal($data)
    {?>
        <?$this->html->RegisterCores();?>
        <?$this->html->SetStyle('styles')?>
        <?$this->html->SetScript('bootstrap.min')?>
        <?$this->html->SetScript('holidays')?>
        <?$this->html->RegisterHead();?>
        <?$this->html->GetFirstBody();?>
        <div class="container-fluid">
            <div class="row">
                <div class="content col-md-12">
                    <ul class="nav nav-pills-stacked">
                        <li role="presentation" class="active"><a href="index.php">terug</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class='container'>
            <div class='row'>
                <div class="col-md-12">
                    <div class="content form">
                        <div class="page-header">
                            <h3>Feestdagen invoeren</h3>
                        </div>
                        <div class='main'>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="content col-md-12">
                                        <ul class="nav nav-pills-stacked">
                                            <li role="presentation" class="active"><a href="index.php">terug</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class='container'>
                                <div class='row'>
                                    <div class="col-md-12">
                                        <div class="content form form-ill">
                                            <div class="page-header">
                                                <h3>Feestdagen invoeren</h3>
                                            </div>
                                            <form>
                                                <fieldset class="form-group">
                                                    <label for="exampleInputEmail1">Feestdag naam</label>
                                                    <input type="text" class="form-control" name="holiday" id="holiday">
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <label for="exampleInputPassword1">Startdatum</label>
                                                    <input type="date" class="form-control" name="startdate" id="add_enddate">
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <label for="exampleInputPassword1">Einddatum</label>
                                                    <input type="date" class="form-control" name="enddate" id="add_startdate">
                                                </fieldset>
                                                <input type="button" id="add_feestdag_submit" class="btn btn-primary">Doorsturen</input>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            </hr>
                            <div class='container'>
                                <div class='row'>
                                    <div class="col-md-12">
                                        <div class="content form form-ill">
                                            <div class="page-header">
                                                <h3>Feestdagen verwijderen</h3>
                                            </div>
                                            <form>
                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Selecteer feestdag</label>
                                                    <select class="form-control" name="select-holiday" id="holiday-select">
                                                        <?php echo $this->dbh->GetSelectHolidays()?>
                                                    </select>
                                                </fieldset>
                                                <button type="submit" class="btn btn-primary">Doorsturen</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            </hr>
                            <div class='container'>
                                <div class='row'>
                                    <div class="col-md-12">
                                        <div class="content form form-ill">
                                            <div class="page-header">
                                                <h3>Feestdagen wijzigen</h3>
                                            </div>
                                            <form>
                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Selecteer feestdag</label>
                                                    <select class="form-control" name="select-holiday" id="holiday-select">
                                                        <?php echo $this->dbh->GetSelectHolidays()?>
                                                    </select>
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <label for="exampleInputPassword1">Startdatum</label>
                                                    <input type="date" class="form-control" name="startdate" >
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <label for="exampleInputPassword1">Einddatum</label>
                                                    <input type="date" class="form-control" name="enddate" >
                                                </fieldset>
                                                <button type="submit" class="btn btn-primary">Doorsturen</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?$this->html->GetLastBody();?>
    <?}
}
?>