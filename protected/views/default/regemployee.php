<?php
class Controller extends Page
{
    public function RenderFinal($data)
    {?>
        <?$this->html->RegisterCores();?>
        <?$this->html->SetStyle('styles')?>
        <?$this->html->SetScript('bootstrap.min')?>
        <?$this->html->RegisterHead();?>
        <?$this->html->GetFirstBody();?>
        <div class='main'>
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-md-12">
                        <ul class="nav nav-pills-stacked">
                            <li role="presentation" class="active"><a href="index.php">terug</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class='container'>
                <div class='row'>
                    <div class="col-md-12">
                        <div class="content form form-ill">
                            <div class="page-header">
                                <h3>Medewerker registratie</h3>
                            </div>
                            <form>
                                <fieldset class="form-group">
                                    <label for="exampleInputEmail1">Voorletter</label>
                                    <input type="text" class="form-control" id="prefix">
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="exampleInputPassword1">Voornaam</label>
                                    <input type="date" class="form-control" id="firstname" >
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="employee">Tussenvoegsel</label>
                                    <input type="text" class="form-control" id="suffix">
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="exampleInputEmail1">Achternaam</label>
                                    <input type="text" class="form-control" id="lastname" >
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="exampleInputPassword1">Email</label>
                                    <input type="text" class="form-control" id="geboortedatum" >
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Afdeling</label>
                                    <select class="form-control" id="department-select">
                                        <?php echo $this->dbh->GetDepartments() ?>
                                    </select>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Permissie</label>
                                    <select class="form-control" id="level-select">
                                        <option value="admin" >administratie</option>
                                        <option value="werknemer" >medewerker</option>
                                        <option value="manager" >manager</option>
                                    </select>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="hoursworked">Contract uren</label>
                                    <input type="text" class="form-control" id="lastname">
                                    <label for="overhours">Deeltijfactor</label>
                                    <input type="text" class="form-control" id="firstname">
                                </fieldset>
                                <input type="button" class="btn btn-primary" value="Registreer medewerker"/>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    <?}
}
?>