<?php
class Controller extends Page
{
    public function RenderFinal($data)
    {?>
        <?$this->html->RegisterCores();?>
        <?$this->html->SetStyle('styles')?>
        <?$this->html->SetScript('bootstrap.min')?>
        <?$this->html->RegisterHead();?>
        <?$this->html->GetFirstBody();?>
        <div class="container-fluid">
            <div class="row">
                <div class="content col-md-12">
                    <ul class="nav nav-pills-stacked">
                        <li role="presentation" class="active"><a href="manager.php">terug</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <h2>Wijzigen project uren</h2>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Projectnaam</th>
                        <th>Aantal uur vast</th>
                        <th>Aantal uur gemaakt</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php echo $this->dbh->GetProjectView(); ?>
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary">Doorsturen</button>
            </div>
        </div>
        <?$this->html->GetLastBody();?>
    <?}
}
?>