<?php
class Controller extends Page
{
    public function RenderFinal($data)
    {?>
        <?$this->html->RegisterCores();?>
        <?$this->html->SetStyle('styles')?>
        <?$this->html->SetScript('bootstrap.min')?>
        <?$this->html->SetScript('vakantie')?>
        <?$this->html->RegisterHead();?>
        <?$this->html->GetFirstBody();?>
        <div class='container'>
            <div class='row'>
                <div class="col-md-12">
                    <div class="content form form-holiday">
                        <div class="page-header">
                            <h3>Vakantie</h3>
                        </div>
                        <form>
                            <fieldset class="form-group">
                                <label for="exampleInputEmail1">Van</label>
                                <input type="text" class="form-control datepicker" name="start-date" id="start_date" >
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="exampleInputPassword1">Tot</label>
                                <input type="text" class="form-control datepicker" name="end-date" id="end_date">
                                <input type="hidden" value="verlof" name="request_type" id="request_type">
                            </fieldset>
                            <input id="vakantie_submit" type="button" class="btn btn-primary" value="Doorsturen">
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <?$this->html->GetLastBody();?>
    <?}
}
?>