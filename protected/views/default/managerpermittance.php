<?php
class Controller extends Page
{
    public function RenderFinal($data)
    {?>
        <?$this->html->RegisterCores();?>
        <?$this->html->SetStyle('styles')?>
        <?$this->html->SetScript('bootstrap.min')?>
        <?$this->html->RegisterHead();?>
        <?$this->html->GetFirstBody();?>
        <div class="container-fluid">
            <div class="row">
                <div class="content col-md-12">
                    <ul class="nav nav-pills-stacked">
                        <li role="presentation" class="active"><a href="manager.php">terug</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <h2>Goedkeuren verlof aanvragen</h2>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Voornaam</th>
                        <th>Achternaam</th>
                        <th>Datum van</th>
                        <th>Datum tot</th>
                        <th>Akkoord</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>John</td>
                        <td>Doe</td>
                        <td>23-03-2016</td>
                        <td>25-09-2016</td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Mary</td>
                        <td>Moe</td>
                        <td>23-03-2016</td>
                        <td>25-04-2016</td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>July</td>
                        <td>Dooley</td>
                        <td>23-07-2016</td>
                        <td>25-12-2016</td>

                    </tr>
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary">Doorsturen</button>
            </div>
        </div>
        <?$this->html->GetLastBody();?>
    <?}
}
?>