<?php
class Controller extends Page
{
    public function RenderFinal($data)
    {?>
        <?$this->html->RegisterCores();?>
        <?$this->html->SetStyle('styles')?>
        <?$this->html->SetScript('bootstrap.min')?>
        <?$this->html->SetScript('ziek')?>
        <?$this->html->RegisterHead();?>
        <?$this->html->GetFirstBody();?>
        <div class='container'>
            <div class='row'>
                <div class="col-md-12">
                    <div class="content form form-ill">
                        <div class="page-header">
                            <h3>Ziek</h3>
                        </div>
                        <form>
                            <fieldset class="form-group">
                                <label for="exampleInputEmail1">Van</label>
                                <input type="text" class="form-control datepicker" id="start-date" >
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="exampleInputPassword1">Tot</label>
                                <input type="text" class="form-control datepicker" id="end-date">
                                <input type="hidden" value="ziek" id="request_type">
                                <input type="hidden" value="0" id="approve">
                            </fieldset>
                            <input id="submit_ziek" type="button" class="btn btn-primary">Doorsturen</input>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?$this->html->GetLastBody();?>
    <?}
}
?>