<?php
class Controller extends Page
{
    public function RenderFinal($data)
    {?>
        <?$this->html->RegisterCores();?>
        <?$this->html->SetStyle('styles')?>
        <?$this->html->SetScript('bootstrap.min')?>
        <?$this->html->RegisterHead();?>
        <?$this->html->GetFirstBody();?>
        <div class='container'>
            <div class='row'>
                <div class="col-md-12">
                    <div class="content form form-holiday">
                        <div class="page-header">
                            <h3>Overzicht</h3>
                        </div>
                        <form>
                            <fieldset class="form-group">
                                <label for="employee">Werknemer</label>
                                <div class="input">
                                    <input type="text" class="form-control employee-first" id="firstname">
                                </div>
                                <div class="input">
                                    <input type="text" class="form-control employee-name" id="lastname">
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="exampleInputEmail1">Van</label>
                                <input type="date" class="form-control" id="exampleInputEmail1">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="exampleInputPassword1">Tot</label>
                                <input type="date" class="form-control" id="exampleInputPassword1">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="hoursworked">gewerkt</label>
                                <input type="text" class="form-control" id="hoursworked">
                                <label for="overhours">overuren</label>
                                <input type="text" class="form-control" id="overhours">
                                <label for="holiday">vakantie</label>
                                <input type="text" class="form-control" id="holiday">
                                <label for="ill">ziek</label>
                                <input type="text" class="form-control" id="ill">

                            </fieldset>
                            <button type="submit" class="btn btn-primary">Doorsturen</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <?$this->html->GetLastBody();?>
    <?}
}
?>