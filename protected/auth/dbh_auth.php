<?php
date_default_timezone_set('Europe/Amsterdam');
error_reporting(E_ALL);
ini_set('log_errors',1);
ini_set('display_errors',1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
class ADbh
{
	private $dataHandle;
	private $gf;

	public function __construct ()
	{
		$result = true;
		//Open connection
		try{
			$this->dataHandle = new PDO('mysql:host=localhost;dbname=framework;charset=utf8', 'root', 'test');
			$this->dataHandle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$result = true;
		}
		catch(mysqli_error $e){
		echo 'Connection failed! Reason: '.$e->getMessage;
		$result = false;
		}
	}
    
    public function Login($email, $pass)
    {
        $result = $this->dataHandle->prepare("SELECT COUNT(*) FROM Users WHERE UserName = ? AND Password = ? AND Activated = 1 AND Blocked = 0");
        $result->execute(array($email, $pass));
        $count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["COUNT(*)"];
        if($final == 1)
        {
            return true;
        }
        else
        {
            echo "nope";
        }
    }
    
    public function Register($username, $email, $password, $regdatetime, $regip, $activated = 0, $act_hash, $act_hash_dt, $blocked = 0)
    {
        $result = $this->dataHandle->prepare("INSERT INTO Users(U_ID, UserName, Email, Password, RegDateTime, RegIP, Activated, ActivationHash, ActivationHashDT, Blocked) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $result->execute(array($username, $email, $password, $regdatetime, $regip, $activated = 0, $act_hash, $act_hash_dt, $blocked = 0));
    }

	public function UIDExists($username)
	{
		$result = $this->dataHandle->prepare("SELECT COUNT(*) FROM User WHERE Email = ?");
		$result->execute(array($username));
		$count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["COUNT(*)"];
		return $final;
	}
    
    public function UserNameExists($username)
	{
        $data = true;
		$result = $this->dataHandle->prepare("SELECT COUNT(*) FROM Users WHERE UserName = ?");
		$result->execute(array($username));
		$count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["COUNT(*)"];
		return $final;
    }
    
    public function EmailExists($username)
	{
        $data = true;
		$result = $this->dataHandle->prepare("SELECT COUNT(*) FROM Users WHERE Email = ?");
		$result->execute(array($username));
		$count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["COUNT(*)"];
		return $final;
    }
    
	public function GetUID($username)
	{
		$result = $this->dataHandle->prepare("SELECT UserID FROM User WHERE Email = ?");
		$result->execute(array($username));
		$count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["UserID"];
		return $final;
	}

	public function LogVisit($datetime, $IP, $OS, $browser, $rawhttp, $continent, $country, $region, $city, $long, $lat)
	{
		$result = $this->dataHandle->prepare("INSERT INTO VisitLogs(ID, DateTime, IP, OS, Browser, RawHTTP, Continent, Country, Region, City, Longtitude, Latitude) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$result->execute(array($datetime, $IP, $OS, $browser, $rawhttp, $continent, $country, $region, $city, $long, $lat));
	}

	public function CheckActivationCode($hash)
    {
        $result = $this->dataHandle->prepare("SELECT ActivationHashDT FROM Users WHERE ActivationHash = ?");
        $result->execute(array($hash));
        $count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = strtotime($count[0]["ActivationHashDT"]);
        if(!empty($final))
        {
            $then = $final;
            $now = time();
            $difference = $now - $then;
            $minutes = floor($difference / 60);
            if($minutes < 15)
            {
                if($this->SetAccountActivated($hash))
                {
                    return true;
                }
            }
        }
		else
        {
            return false;
        }
    }
    
    public function SetAccountActivated($hash)
    {
        $nope = '';
        $result = $this->dataHandle->prepare("UPDATE Users SET ActivationHash = null, Activated = 1 WHERE ActivationHash = ?");
        if($result->execute(array($hash)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function DeleteByEmail($email)
    {
        $result = $this->dataHandle->prepare("DELETE FROM Users WHERE Email = ?");
        $result->execute(array($email));
    }
    
    public function SetPassRecovery($email, $hash, $date_time)
    {
        $ID = $this->GetIDByEmail($email);
        $result = $this->dataHandle->prepare("INSERT INTO PasswordReset(ID, U_ID, DateTime, ClickDateTime, ResetHash) VALUES(NULL, ?, ?, NULL, ?)");
        $result->execute(array($ID, $date_time, $hash));
        $result = $this->dataHandle->prepare("UPDATE Users SET Blocked = 1 WHERE Email = ?");
        $result->execute(array($email));
        return true;
    }
    
    public function AccountActivated($email)
    {
        $result = $this->dataHandle->prepare("SELECT COUNT(*) FROM Users WHERE Email = ? AND Activated = 1");
        $result->execute(array($email));
        $check = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $check[0]["COUNT(*)"];
        if($final == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function PassResetExists($email)
    {
        $ID = $this->GetIDByEmail($email);
        //echo "ID; $ID/";
        $result = $this->dataHandle->prepare("SELECT DateTime FROM PasswordReset WHERE U_ID = ? AND ClickDateTime IS NULL ORDER BY ID DESC LIMIT 1");
        $result->execute(array($ID));
        $check = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $check[0]["DateTime"];
        //echo "FINAL: $final--";
        if($final !== "")
        {
            $then = strtotime($final);
            $now = time();
            $difference = $now - $then;
            $minutes = floor($difference / 60);
            if($minutes < 15)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return false;
    }
    
    public function PassResetValid($hash)
    {
        $result = $this->dataHandle->prepare("SELECT DateTime FROM PasswordReset WHERE ResetHash = ? AND ClickDateTime IS NULL ORDER BY ID DESC LIMIT 1");
        $result->execute(array($hash));
        $check = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $check[0]["DateTime"];
        //echo "FINAL: $final--";
        if($final !== "")
        {
            $then = strtotime($final);
            $now = time();
            $difference = $now - $then;
            $minutes = floor($difference / 60);
            if($minutes < 15)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return false;
    }
    
    public function SubmitResetValid($hash)
    {
        $result = $this->dataHandle->prepare("SELECT ClickDateTime FROM PasswordReset WHERE ResetHash = ? ORDER BY ID DESC LIMIT 1");
        $result->execute(array($hash));
        $check = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $check[0]["ClickDateTime"];
        //echo "FINAL: $final--";
        if($final !== "")
        {
            $then = strtotime($final);
            $now = time();
            $difference = $now - $then;
            $minutes = floor($difference / 60);
            if($minutes < 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return false;
    }
    
    public function UpdateResetTime($hash)
    {
        $date = date('Y-m-d H:i:s');
        $result = $this->dataHandle->prepare("UPDATE PasswordReset SET ClickDateTime = ? WHERE ResetHash = ?");
        $result->execute(array($date, $hash));
    }
    
    public function GetIDByEmail($email)
    {
        $result = $this->dataHandle->prepare("SELECT U_ID FROM Users WHERE Email = ?");
        $result->execute(array($email));
        $mail = $result->fetchAll(PDO::FETCH_ASSOC);
        $ID = $mail[0]["U_ID"];
        //echo "U_ID: $ID";
        return $ID;
    }
    
    public function GetIDByHash($hash)
    {
        $result = $this->dataHandle->prepare("SELECT U_ID FROM PasswordReset WHERE ResetHash = ?");
        $result->execute(array($hash));
        $mail = $result->fetchAll(PDO::FETCH_ASSOC);
        $ID = $mail[0]["U_ID"];
        //echo "U_ID: $ID";
        return $ID;
    }

    public function UpdatePassword($hash, $password)
    {
        $uid = $this->GetIDByHash($hash);
        if(!empty($uid))
        {
            if($this->ActuallyChangePassword($password, $uid))
            {
                if($this->UnblockUser($uid))
                {
                }else{return false;}
            }else{return false;}
        }else{return false;}
    }
    
    public function ActuallyChangePassword($pass, $uid)
    {
        $result = $this->dataHandle->prepare("UPDATE Users SET Password = ? WHERE U_ID = ?");
        $result->execute(array($pass, $uid));
        return true;
    }
    
    /*public function RemovePasswordReset($hash)
    {
        $result = $this->dataHandle->prepare("DELETE FROM PasswordReset WHERE ResetHash = ?");
        $result->execute(array($hash));
        return true;
    }*/
    
    public function UnblockUser($uid)
    {
        $result = $this->dataHandle->prepare("UPDATE Users SET Blocked = 0 WHERE U_ID = ?");
        $result->execute(array($uid));
        return true;
    }
    
    function date_difference ($date1timestamp, $date2timestamp)
    {
   $all = round(($date1timestamp - $date2timestamp) / 60);
   $d = floor ($all / 1440);
   $h = floor (($all - $d * 1440) / 60);
   $m = $all - ($d * 1440) - ($h * 60);

   return array('d' => $d, 'h' => $h, 'm' => $m);
}
}
?>
