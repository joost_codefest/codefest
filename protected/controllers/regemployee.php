<?php
/**
 * Created by PhpStorm.
 * User: jorrit
 * Date: 24-3-16
 * Time: 10:29
 */
class Page extends Framework
{
    public function InitPage($p)
    {
        //var_dump(get_object_vars($this));
        require_once($this->base_dir."protected/views/default/".$p.".php");
        $c = new Controller(true);
        $c->RenderFinal(null);
    }
}
?>