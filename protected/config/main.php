<?php
class Config
{
    //DB user
    public $dbuser = 'root';

    //DB password
    public $dbpass = 'test';

    //DB host
    public $dbhost = 'localhost';

    //DB name
    public $dbdb   = 'db';

    //Base directory
    public $base_dir = '/var/www/codefest/';
    
    //Array with auth protected pages
    public $secrets = array(
        'goedkeuren',
        'ziek',
        'uren',
        'administrativeemployee',
        'manager',
        'managerpermittance',
        'managerprojecthours',
        'overzicht',
        'vakantie',
        'holidays',
    );
    
    public $req_modules = array(
        'dbh.php',
        'html.php',
        'menu.php',
    );

    public $PageLevels = array(
        'employee' => array(
            'ziek',
            'vakantie',
            'uren',
        ),
        'manager' => array(
            'managerpermittance',
            'managerprojecthours',
            'overzicht',
            'manager',
        ),
        'administratie' => array(
            'administrativeemployee',
            'holidays',
        )
    );

    public $FunctionLevels = array(
        'employee' => array(
            'request_freedays',
            'callinsick',
            'reg_individial_project_hours',
            'register_ovework',
            'buy_vacation_days',
        ),
        'administratie' => array(
            'register_new_employee',
            'change_employe_data',
            'deactivate_employee',
            'register_bhds',
            'register_presence',
            'register_partime_factor',
        ),
        'manager' => array(
            'accept_deny_vacation',

        )
    );
}
?>