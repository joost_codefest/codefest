<?php
session_start();
class ULevel extends Framework
{
    public function TestLevel()
    {
        echo $this->base_dir;
    }

    public function CheckLevel($dbh, $page)
    {
        $c = false;
        $level = $this->GetUserLevel($dbh);
        foreach($this->PageLevels[$level] as $l)
        {
            if($l == $page)
            {
                $c = true;
            }
        }
        return $c;
    }
    
    public function CheckFunctionLevel($dbh, $f, $function)
    {
        $c = false;
        $level = $this->GetUserLevel($dbh);
        foreach($f[$level] as $l)
        {
            if($l == $function)
            {
                $c = true;
            }
        }
        return $c;
    }

    public function GetUserLevel($dbh)
    {
        return $dbh->GetULByID($_SESSION['ID']);
    }
}
?>