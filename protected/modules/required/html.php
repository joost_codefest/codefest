<?php
class Html
{
    private $Content;
    private $Scripts;
    private $Styles;
    private $Head;
    private $BodyScripts;
    
    public function TestRender()
    {
        echo "RENDER WORKS WITH DOL";
    }
    
    public function RegisterCores()
    {
        $this->Scripts[] = 'jquery';
        $this->Scripts[] = 'jquery-ui.min';
        $this->Scripts[] = 'main';
        $this->Styles[] = 'bootstrap.min';
        $this->Styles[] = 'jquery-ui.min';
        $this->Styles[] = 'font-awesome';
    }
    
    public function RegisterHead()
    {
        $this->Head .= "
            <!DOCTYPE html>
            <head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
            <!--Javascript files-->
            ";foreach($this->Scripts as $s)
            {
                $this->Head .= "<script src='js/".$s.".js'></script>\n\t\t\t";
            }
                
            $this->Head .= "<!--CSS files-->\n\t\t\t";
            foreach($this->Styles as $style)
            {
            $this->Head .= "<link rel='stylesheet' type='text/css' href='css/".$style.".css'>\n\t\t\t";
            }
            $this->Head .= '</head>
        ';
        echo $this->Head;
    }
    
    public function GetFirstBody()
    {
        echo '<body>
                ';
                if(!empty($this->BodyScripts))
                {
                    foreach($this->BodyScripts as $s)
                    {
                        echo "<script src='js/".$s.".js'></script>\n\t\t\t";
                    }
                }
                echo '<div id="main_content">';
    }
    
    public function GetLastBody()
    {
        echo "\n\t\t\t\t</div>
            </body>";
    }
    
    public function render()
    {?>
    <!DOCTYPE html>
    <head>
        <!--Register all script files-->
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <?
        foreach($this->Scripts as $script)
        {
            echo "<script src='js/$script.js' type='text/javascript'></script>";
        }
        ?>
        
        
        <!--Register all styles-->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <?
        foreach($this->Styles as $style)
        {
            echo "<link rel='stylesheet' type='text/css' href='css/$style.css'>";
        }
        ?>
    </head>
    <body>
        <div id="main_content">
        <?=$this->Content;?>
        </div>
    </body>
    </html>

    <?}
    
    public function SetScript($script)
    {
        $this->Scripts[] = $script;
    }
    
    public function SetBodyScript($script)
    {
        $this->BodyScripts[] = $script;
    }
    
    public function SetStyle($style)
    {
        $this->Styles[] = $style;
    }
    
    public function SetContent($c)
    {
        $this->Content = $content;
    }
}
?>