<?php
date_default_timezone_set('Europe/Amsterdam');
error_reporting(E_ALL);
ini_set('log_errors',1);
ini_set('display_errors',1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
class Dbh
{
	private $dataHandle;
	private $gf;

	public function __construct ()
	{
		$result = true;
		//Open connection
		try{
			$this->dataHandle = new PDO('mysql:host=localhost;dbname=test2;charset=utf8', 'root', 'test');
			$this->dataHandle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$result = true;
		}
		catch(mysqli_error $e){
		echo 'Connection failed! Reason: '.$e->getMessage;
		$result = false;
		}
	}
    
    public function Login($email, $pass)
    {
        $result = $this->dataHandle->prepare("SELECT COUNT(*) FROM Users WHERE Email = ? AND Password = ? AND Activated = 1 AND Blocked = 0");
        $result->execute(array($email, $pass));
        $count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["COUNT(*)"];
        if($final == 1)
        {
            return true;
        }
        else
        {
            echo "nope";
        }
    }
    
    public function Register($username, $email, $password, $regdatetime, $regip, $activated = 0, $act_hash, $act_hash_dt, $blocked = 0)
    {
        $result = $this->dataHandle->prepare("INSERT INTO Users(U_ID, UserName, Email, Password, RegDateTime, RegIP, Activated, ActivationHash, ActivationHashDT, Blocked) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $result->execute(array($username, $email, $password, $regdatetime, $regip, $activated = 0, $act_hash, $act_hash_dt, $blocked = 0));
    }

	public function UIDExists($username)
	{
		$result = $this->dataHandle->prepare("SELECT COUNT(*) FROM User WHERE Email = ?");
		$result->execute(array($username));
		$count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["COUNT(*)"];
		return $final;
	}
    
    public function UserNameExists($username)
	{
        $data = true;
		$result = $this->dataHandle->prepare("SELECT COUNT(*) FROM Users WHERE UserName = ?");
		$result->execute(array($username));
		$count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["COUNT(*)"];
		return $final;
    }
    
    public function EmailExists($username)
	{
        $data = true;
		$result = $this->dataHandle->prepare("SELECT COUNT(*) FROM Users WHERE Email = ?");
		$result->execute(array($username));
		$count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["COUNT(*)"];
		return $final;
    }
    
	public function GetUID($username)
	{
		$result = $this->dataHandle->prepare("SELECT UserID FROM User WHERE Email = ?");
		$result->execute(array($username));
		$count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $count[0]["UserID"];
		return $final;
	}

	public function LogVisit($datetime, $IP, $OS, $browser, $rawhttp, $continent, $country, $region, $city, $long, $lat)
	{
		$result = $this->dataHandle->prepare("INSERT INTO VisitLogs(ID, DateTime, IP, OS, Browser, RawHTTP, Continent, Country, Region, City, Longtitude, Latitude) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$result->execute(array($datetime, $IP, $OS, $browser, $rawhttp, $continent, $country, $region, $city, $long, $lat));
	}

	public function CheckActivationCode($hash)
    {
        $result = $this->dataHandle->prepare("SELECT ActivationHashDT FROM Users WHERE ActivationHash = ?");
        $result->execute(array($hash));
        $count = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = strtotime($count[0]["ActivationHashDT"]);
        if(!empty($final))
        {
            $then = $final;
            $now = time();
            $difference = $now - $then;
            $minutes = floor($difference / 60);
            if($minutes < 15)
            {
                if($this->SetAccountActivated($hash))
                {
                    return true;
                }
            }
        }
		else
        {
            return false;
        }
    }
    
    public function SetAccountActivated($hash)
    {
        $nope = '';
        $result = $this->dataHandle->prepare("UPDATE Users SET ActivationHash = null, Activated = 1 WHERE ActivationHash = ?");
        if($result->execute(array($hash)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function DeleteByEmail($email)
    {
        $result = $this->dataHandle->prepare("DELETE FROM Users WHERE Email = ?");
        $result->execute(array($email));
    }
    
    public function SetPassRecovery($email, $hash, $date_time)
    {
        $ID = $this->GetIDByEmail($email);
        $result = $this->dataHandle->prepare("INSERT INTO PasswordReset(ID, U_ID, DateTime, ClickDateTime, ResetHash) VALUES(NULL, ?, ?, NULL, ?)");
        $result->execute(array($ID, $date_time, $hash));
        $result = $this->dataHandle->prepare("UPDATE Users SET Blocked = 1 WHERE Email = ?");
        $result->execute(array($email));
        return true;
    }
    
    public function AccountActivated($email)
    {
        $result = $this->dataHandle->prepare("SELECT COUNT(*) FROM Users WHERE Email = ? AND Activated = 1");
        $result->execute(array($email));
        $check = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $check[0]["COUNT(*)"];
        if($final == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function PassResetExists($email)
    {
        $ID = $this->GetIDByEmail($email);
        //echo "ID; $ID/";
        $result = $this->dataHandle->prepare("SELECT DateTime FROM PasswordReset WHERE U_ID = ? AND ClickDateTime IS NULL ORDER BY ID DESC LIMIT 1");
        $result->execute(array($ID));
        $check = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $check[0]["DateTime"];
        //echo "FINAL: $final--";
        if($final !== "")
        {
            $then = strtotime($final);
            $now = time();
            $difference = $now - $then;
            $minutes = floor($difference / 60);
            if($minutes < 15)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return false;
    }
    
    public function PassResetValid($hash)
    {
        $result = $this->dataHandle->prepare("SELECT DateTime FROM PasswordReset WHERE ResetHash = ? AND ClickDateTime IS NULL ORDER BY ID DESC LIMIT 1");
        $result->execute(array($hash));
        $check = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $check[0]["DateTime"];
        //echo "FINAL: $final--";
        if($final !== "")
        {
            $then = strtotime($final);
            $now = time();
            $difference = $now - $then;
            $minutes = floor($difference / 60);
            if($minutes < 15)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return false;
    }
    
    public function SubmitResetValid($hash)
    {
        $result = $this->dataHandle->prepare("SELECT ClickDateTime FROM PasswordReset WHERE ResetHash = ? ORDER BY ID DESC LIMIT 1");
        $result->execute(array($hash));
        $check = $result->fetchAll(PDO::FETCH_ASSOC);
        $final = $check[0]["ClickDateTime"];
        //echo "FINAL: $final--";
        if($final !== "")
        {
            $then = strtotime($final);
            $now = time();
            $difference = $now - $then;
            $minutes = floor($difference / 60);
            if($minutes < 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return false;
    }
    
    public function UpdateResetTime($hash)
    {
        $date = date('Y-m-d H:i:s');
        $result = $this->dataHandle->prepare("UPDATE PasswordReset SET ClickDateTime = ? WHERE ResetHash = ?");
        $result->execute(array($date, $hash));
    }
    
    public function GetIDByEmail($email)
    {
        $result = $this->dataHandle->prepare("SELECT U_ID FROM Users WHERE Email = ?");
        $result->execute(array($email));
        $mail = $result->fetchAll(PDO::FETCH_ASSOC);
        $ID = $mail[0]["U_ID"];
        //echo "U_ID: $ID";
        return $ID;
    }
    
    public function GetIDByHash($hash)
    {
        $result = $this->dataHandle->prepare("SELECT U_ID FROM PasswordReset WHERE ResetHash = ?");
        $result->execute(array($hash));
        $mail = $result->fetchAll(PDO::FETCH_ASSOC);
        $ID = $mail[0]["U_ID"];
        //echo "U_ID: $ID";
        return $ID;
    }

    public function UpdatePassword($hash, $password)
    {
        $uid = $this->GetIDByHash($hash);
        if(!empty($uid))
        {
            if($this->ActuallyChangePassword($password, $uid))
            {
                if($this->UnblockUser($uid))
                {
                }else{return false;}
            }else{return false;}
        }else{return false;}
    }
    
    public function ActuallyChangePassword($pass, $uid)
    {
        $result = $this->dataHandle->prepare("UPDATE Users SET Password = ? WHERE U_ID = ?");
        $result->execute(array($pass, $uid));
        return true;
    }
    
    /*public function RemovePasswordReset($hash)
    {
        $result = $this->dataHandle->prepare("DELETE FROM PasswordReset WHERE ResetHash = ?");
        $result->execute(array($hash));
        return true;
    }*/
    
    public function UnblockUser($uid)
    {
        $result = $this->dataHandle->prepare("UPDATE Users SET Blocked = 0 WHERE U_ID = ?");
        $result->execute(array($uid));
        return true;
    }
    
    function date_difference ($date1timestamp, $date2timestamp)
    {
   $all = round(($date1timestamp - $date2timestamp) / 60);
   $d = floor ($all / 1440);
   $h = floor (($all - $d * 1440) / 60);
   $m = $all - ($d * 1440) - ($h * 60);

   return array('d' => $d, 'h' => $h, 'm' => $m);
    }

    public function GetULByID($id)
    {
        $result = $this->dataHandle->prepare("SELECT Level FROM Users WHERE U_ID = ?");
        $result->execute(array($id));
        $tmp = $result->fetchAll(PDO::FETCH_ASSOC);
        $level = $tmp[0]["Level"];
        return $level;
    }

    /*Database Functions jorrit*/
    public function LoginRegister ($username, $password, $employee){
        $result = $this->dataHandle->prepare("INSERT INTO Inloggegevens(idInloggegevens, Gebruiksnaam, Wachtwoord, Medewerkers_idMedewerkers ) VALUES(NULL, ?, ?, ?)");
        $result->execute(array($username, $password, $employee));
    }

    public function RegisterEmployee($voorletter, $voornaam, $tussenvoegsel, $achternaam, $email, $afdeling, $contracturen, $deeltijdfactor) {
        $result = $this->dataHandle->prepare("INSERT INTO Medewerkers(idMedewerkers, Voorletter, Voornaam, Tussenvoegsel, Achternaam, Email, Afdelingen_idAfdeling, Contracturen, Deeltijdfactor) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $result->execute(array($voorletter, $voornaam, $tussenvoegsel, $achternaam, $email, $afdeling, $contracturen, $deeltijdfactor));
    }

    public function RequestPermittance($startdatum, $eindatum, $type, $akkoord) {
        $result = $this->dataHandle->prepare("INSERT INTO Verlof(idVerlof, Startdatum, Einddatum, Type, Akkoord) VALUES(NULL, ?, ?, ?, ?)");
        $result->execute(array($startdatum, $eindatum, $type, $akkoord));
    }

    public function SetIll($startdatum, $eindatum, $type, $akkoord) {
        $result = $this->dataHandle->prepare("INSERT INTO Verlof(idVerlof, Startdatum, Einddatum, Type, Akkoord) VALUES(NULL, ?, ?, ?, ?, ?");
        $result->execute(array($startdatum, $eindatum, $type, $akkoord));
    }
    public function AddHoliday($naam, $startdatum, $eindatum) {
        $result = $this->dataHandle->prepare("INSERT INTO Feestdagen(idFeestdagen, Naam, Startdatum, Einddatum) VALUES(NULL, ?, ?, ?)");
        $result->execute(array($naam,$startdatum, $eindatum));
    }



    public function GetEmployeeID ($username) {
        $result = $this->dataHandle->prepare("SELECT idMedewerkers FROM Medewerkers WHERE Email = ?");
        $result->execute(array($username));

        while ($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            $id = $row['idMedewerkers'];

            return $id;
        }
    }



    public function GetAllEmployeeNames()
    {
        $option = '';
        $result = $this->dataHandle->prepare("SELECT idMedewerkers, Voornaam, Tussenvoegsel, Achternaam FROM Medewerkers");
        $result->execute();

        while ($row = $result->fetch()) {
            $option .= '<option value="'.$row['idMedewerkers'].'">'.$row['Tussenvoegsel'].' '.$row['Achternaam'].', '.$row['Voornaam'].'</option>';
        }

        return $option;
    }

    public function GetProfileInformation($username){
        $profile_info = array();
        $result = $this->dataHandle->prepare("SELECT M.Voornaam, M.Tussenvoegsel, M.Achternaam, M.Geslacht, M.Geboortedatum, M.Contracturen, A.Afdeling FROM Medewerkers M INNER JOIN Afdelingen A ON M.Afdelingen_idAfdeling = A.idAfdeling WHERE Email = ?");
        $result->execute(array($username));

        while($row = $result->fetch()) {
            $firstname = $row['Voornaam'];
            $infix = $row['Tussenvoegsel'];
            $lastname = $row['Achternaam'];
            $employee_dept = $row['Afdeling'];

            array_push($profile_info, $firstname, $infix, $lastname, $employee_dept);
        }

        return $profile_info;
    }

    public function GetProjects(){
        $option = ' ';
        $result = $this->dataHandle->prepare("SELECT * FROM Projecten");
        $result->execute();

        while($row = $result->fetchAll())	{
            $option .= '<option value="'.$row[0]['idProjecten'].'">'.$row[0]['Projectnaam'].'</option>';
        }

        return $option;
    }

    public function GetProjectView(){
        $i = 0;
        $table =' ';
        $result = $this->dataHandle->prepare("SELECT * FROM Projecten");
        $result->execute();

        while($row = $result->fetch())	{
            $table .= '<tr>
			<td>'.$row['Naam'].'</td>
			<td>'.$row['Geplande_uren'].'</td>
			<td>'.$row['MGewerkte_uren'].'</td>
			</tr>';
            $i++;
        }
        return $table;
    }

    public function GetAllHolidays(){
        $holidays = array();
        $result = $this->dataHandle->prepare("SELECT * FROM Feestdagen");
        $result->execute();

        while($row = $result->fetch()) {
            $holiday_name = $row['Naam'];
            $start_date = $row['Startdatum'];
            $end_date = $row['Einddatum'];

            array_push($holidays, $holiday_name, $start_date. $end_date);
        }

        return $holidays;
    }

    public function GetDepartments(){
        $option = ' ';
        $i = 0;
        $result = $this->dataHandle->prepare("SELECT * FROM Afdelingen");
        $result->execute();

        while($row = $result->fetch()) {
            $option .= '<option value="'.$row['idAfdelingen'].'">'.$row['Naam'].'</option>';
            $i++;
        }

        return $option;
    }


    public function GetDepartmentView(){
        $i = 0;
        $table =' ';
        $result = $this->dataHandle->prepare("SELECT * FROM Afdelingen");
        $result->execute();

        while($row = $result->fetch())	{
            $table .= '<tr>
			<td>'.$row['Afdeling'].'</td>
			<td>'.$row['Huidige_bezetting'].'</td>
			<td>'.$row['Minimum_bezetting'].'</td>
			</tr>';
            $i++;
        }
        return $table;
    }

    public function GetSelectHolidays(){
        $option = ' ';
        $i = 0;
        $result = $this->dataHandle->prepare("SELECT * FROM Feestdagen");
        $result->execute();

        while($row = $result->fetch()) {
            $option .= '<option value="'.$row['idFeestdagen'].'">'.$row['Naam'].'</option>';
            $i++;
        }

        return $option;
    }

    public function RemoveHoliday($id){
        $result = $this->dataHandle->prepare("DELETE FROM Feestdagen WHERE idFeestdagen = ?");
        $result->execute($id);
    }

    public function UpdateHoliday($id, $naam, $start_date, $end_date){
        $result = $this->dataHandle->prepare("UPDATE Feestdagen SET Naam = ?, Startdatum = ? , Einddatum = ? WHERE idFeestdagen = ?");
        $result->execute(array($id, $naam, $start_date, $end_date));
    }

    public function GetPermittanceID($id){
        $request_ids = array();
        $result = $this->dataHandle->prepare("SELECT * FROM Verlof");
        $result->execute(array($id));
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $id = $row['idVerlof'];

            array_push($request_ids, $id);
        }
        return $id;
    }
    public function ViewPermittanceEmployee($id){
        $dbh = new DataHandler;
        $a_id = $dbh->GetPermittanceID($id);
        $i = 0;
        $table = '';
        $result = $this->dataHandle->prepare("SELECT V.idVerlof, V.Startdatum, V.Einddatum, V.Type, V.Akkoord, M.Voornaam, M.Achternaam, FROM Verlof V INNER JOIN Medewerkers M ON M.Medewerkers_idMedewerkers = M.idMedewerkers WHERE M.Medewerkers_idMedewerkers = ? ");
        $result->execute(array($id));
        while($row = $result->fetch(PDO::FETCH_ASSOC))	{
            $table .= '<tr>
			<td>'.$row['Voornaam'].'</td>
			<td>'.$row['Achternaam'].'</td>
			<td>'.$row['Type'].'</td>
			<td>'.$row['Startdatum'].'</td>
			<td>'.$row['Einddatum'].'</td>
			<td>'.$row['Akkoord'].'</td>
			<td><a href="?page=managerpermittance&VerlofID='.$a_id[$i].'" class="btn btn-warning">Wijzig Aanvraag</a></td>
			</tr>';
            $i++;
        }
        return $table;
    }
}
?>