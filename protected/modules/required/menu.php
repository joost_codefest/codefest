<?php
session_start();
class Menu
{
    public function GetMenu()
    {
        if(empty($_SESSION['ID']))
        {
            $this->GetGeneralMenu();
        }
        else
        {
            $this->GetAuthMenu();
        }
       
    }
    
    private function GetAuthMenu()
    {?>
    
        <div class='header'>
            <div class='container-fluid'>
                <div class='row'>
                    <div class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                                    <li><a href="manager.php">Manager</a></li>
                                    <li><a href="aministrative_employee.php">Administrative</a></li>
                                    <li><a href="employee.php">Link</a></li>
                                    <li><a href="#">Link</a></li>
                                </ul>
                                <div class="nav navbar-nav navbar-right">
                                    <a class="navbar-brand" href="#"><span class='glyphicon glyphicon-user user-icon'></span></a>
                                </div>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </div>
                </div>
            </div>
        </div>
    <?}
    
    private function GetGeneralMenu()
    {?>
        <div class="bs-example">
    <nav role="navigation" class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Brand</a>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Profile</a></li>
                <li><a href="#">Messages</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Login</a></li>
            </ul>
        </div>
    </nav>
</div>
    <?}
}
?>