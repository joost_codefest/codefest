<?php
class Security
{
    public function HashPassword($password)
    {
        $username = 'MXTyLMRy8JKzphruL0NoKOSmsXezSN1rvtT4aWhq76R26p2R1tOfMM7ciIq3nyOB';
        $salt;
        for($i=0;$i<1500;$i++)
        {
            $username = hash('sha512', $username);
            $salt = $username;
        }
        $password = crypt($password, '$2a$12$' . $salt);
        return $password;
    }
    
    function GetAcHash($length = 100) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
?>