<?php

//Make sure that any still initiated/running sessions are terminated.
session_start();
session_destroy();
session_start();

$_SESSION['Hash'] = $_GET['hash'];

require_once("./protected/auth/dbh_auth.php");
$dbh = new ADbh();
$check = $dbh->PassResetValid($_GET['hash']);
if($check)
{
    require_once("Includes/resetClass.php");
    require_once("Includes/HtmlPageClass.php");
    $date_time = date('Y-m-d H:i:s');
    $dbh->UpdateResetTime($_GET['hash'], $date_time);
    $page = new HtmlPage();
    $reset = new Reset();
    $page->SetContent($reset->render());
    echo $page->render();
}
else
{
    echo "Link has expired, directing back to the login page...
    <script>
    setTimeout(function()
    {
    window.location = 'index.php';
    });
    </script>
    ";
}
?>