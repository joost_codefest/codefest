<?php
//session_start();
error_reporting(E_ALL);
ini_set('log_errors',1);
ini_set('display_errors',1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
class DataHandler
{
    private $dataHandle;
    private $gf;

    public function __construct ()
    {
        $result = true;
        //Open connection
        try{
            $this->dataHandle = new PDO('mysql:host=localhost;dbname=Database_Ziekenhuis_DeWeeren;charset=utf8', 'jorrit', 'nopenope');
            $this->dataHandle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $result = true;
        }
        catch(mysqli_error $e){
            echo 'Connection failed! Reason: '.$e->getMessage;
            $result = false;
        }
    }


    public function GetNameForHeader($username)
    {
        $temp;
        $result = $this->dataHandle->prepare("SELECT Voorletter, Tussenvoegsel, Achternaam FROM Medewerkers WHERE Email = ?");
        $result->execute(array($username));
        while ($row = $result->fetch()) {
            if($row['Tussenvoegsel'] != "") {
                $temp = $row['Voorletter'].". ".$row['Tussenvoegsel']. " ". $row['Achternaam'];
            } else {
                $temp = $row['Voorletter'].". ". $row['Achternaam'];
            }

        }
        return $temp;
    }

    //DBH F1 and F2: Employee and Login registration
    public function LoginRegister ($username, $password, $employee){
        $result = $this->dataHandle->prepare("INSERT INTO Inloggegevens(idInloggegevens, Gebruiksnaam, Wachtwoord, Medewerkers_idMedewerkers ) VALUES(NULL, ?, ?, ?)");
        $result->execute(array($username, $password, $employee));
    }

    public function RegisterEmployee($voorletter, $voornaam, $tussenvoegsel, $achternaam, $email, $afdeling, $contracturen, $deeltijdfactor) {
        $result = $this->dataHandle->prepare("INSERT INTO Medewerkers(idMedewerkers, Voorletter, Voornaam, Tussenvoegsel, Achternaam, Email, Afdelingen_idAfdeling, Contracturen, Deeltijdfactor) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $result->execute(array($voorletter, $voornaam, $tussenvoegsel, $achternaam, $email, $afdeling, $contracturen, $deeltijdfactor));
    }

    public function RequestPermittance($startdatum, $eindatum, $type, $akkoord) {
        $result = $this->dataHandle->prepare("INSERT INTO Verlof(idVerlof, Startdatum, Eindatum, Type, Akkoord) VALUES(NULL, ?, ?, ?, ?, ?");
        $result->execute(array($startdatum, $eindatum, $type, $akkoord));
    }

    public function SetIll($startdatum, $eindatum, $type, $akkoord) {
        $result = $this->dataHandle->prepare("INSERT INTO Verlof(idVerlof, Startdatum, Eindatum, Type, Akkoord) VALUES(NULL, ?, ?, ?, ?, ?");
        $result->execute(array($startdatum, $eindatum, $type, $akkoord));
    }

    

    public function GetEmployeeID ($username) {
        $id;
        $result = $this->dataHandle->prepare("SELECT idMedewerkers FROM Medewerkers WHERE Email = ?");
        $result->execute(array($username));

        while ($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            $id = $row['idMedewerkers'];

            return $id;
        }
    }



    public function GetAllEmployeeNames()
    {
        $option = '';
        $result = $this->dataHandle->prepare("SELECT idMedewerkers, Voornaam, Tussenvoegsel, Achternaam FROM Medewerkers");
        $result->execute(array($functieid));

        while ($row = $result->fetch()) {
            $option .= '<option value="'.$row['idMedewerkers'].'">'.$row['Tussenvoegsel'].' '.$row['Achternaam'].', '.$row['Voornaam'].'</option>';
        }

        return $option;
    }

    public function GetProfileInformation($username){
        $profile_info = array();
        $result = $this->dataHandle->prepare("SELECT M.Voornaam, M.Tussenvoegsel, M.Achternaam, M.Geslacht, M.Geboortedatum, M.Contracturen, A.Afdeling FROM Medewerkers M INNER JOIN Afdelingen A ON M.Afdelingen_idAfdeling = A.idAfdeling WHERE Email = ?");
        $result->execute(array($username));

        while($row = $result->fetch()) {
            $firstname = $row['Voornaam'];
            $infix = $row['Tussenvoegsel'];
            $lastname = $row['Achternaam'];
            $employee_dept = $row['Afdeling'];

            array_push($profile_info, $firstname, $infix, $lastname, $employee_dept);
        }

        return $profile_info;
    }

    public function GetHolidays(){
        $profile_info = array();
        $result = $this->dataHandle->prepare("SELECT * FROM Feestdagen");
        $result->execute(array($username));

        while($row = $result->fetch()) {
            $holiday_name = $row['Naam'];
            $start_date = $row['Startdatum'];
            $end_date = $row['Einddatum'];

            array_push($holidays, $holiday_name, $start_date. $end_date);
        }

        return $holidays;
    }
    public function GetPermittanceID($id){
        $result = $this->dataHandle->prepare("SELECT * FROM Verlof");
        $result->execute(array($id));
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $id = $row['idVerlof'];

            return $id;
        }
    }
    public function ViewPermittanceEmployee($id){
        $dbh = new DataHandler;
        $a_id = $dbh->GetPermittanceID($id);
        $i = 0;
        $table;
        $result = $this->dataHandle->prepare("SELECT V.idVerlof, V.Startdatum, V.Einddatum, V.Type, V.Akkoord, M.Voornaam, M.Achternaam, FROM Verlof V INNER JOIN Medewerkers M ON M.Medewerkers_idMedewerkers = M.idMedewerkers WHERE M.Medewerkers_idMedewerkers = ? ");
        $result->execute(array($id));
        while($row = $result->fetch())	{
            $table .= '<tr>
			<td>'.$row['Voornaam'].'</td>
			<td>'.$row['Achternaam'].'</td>
			<td>'.$row['Type'].'</td>
			<td>'.$row['Startdatum'].'</td>
			<td>'.$row['Einddatum'].'</td>
			<td>'.$row['Akkoord'].'</td>
			<td><a href="?page=aanvraag_wijzigen&VerlofID='.$a_id[$i].'" class="btn btn-warning">Wijzig Aanvraag</a></td>
			</tr>';
            $i++;
        }
        return $table;
    }

}

/**
 * Insurrance querie for packages
 * SELECT P.Naam, P.Type, P.Omschrijving, V.Naam FROM Pakketten P INNER JOIN Verzekeraar V ON V.idVerzekeraar = P.Verzekeraar_idVerzekeraar WHERE P.Verzekeraar_idVerzekeraar = 1
 *
 */
/**
 * Created by PhpStorm.
 * User: jorrit
 * Date: 23-3-16
 * Time: 23:10
 */