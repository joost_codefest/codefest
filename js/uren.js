$(document).ready(function()
{
    $("#submit_ziek").click(function(e)
    {
        e.preventDefault();
        e.stopPropagation();

        var data = {"project_select":document.getElementById("project_select"), "hours_worked":document.getElementById("hours_worked"), "hours_extended":document.getElementById("hours_extended")};

        $.ajax({
            type: "POST",
            url: data,
            data: {"type" : "callinsick", "data" : data},
            dataType: "JSON",
            cache: false,
            success: function(result)
            {
                alert(result);
            },
            error: function (req, status, error)
            {
                alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
            }
        });
    });
});