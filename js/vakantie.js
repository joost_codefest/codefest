$(document).ready(function()
{
    $("#vakantie_submit").click(function(e)
    {
        e.preventDefault();

        var data = {"start_date":document.getElementById("start_date"), "end_date":document.getElementById("end_date"), "request_type":document.getElementById("request_type"), "approve":document.getElementById("approve")};

        $.ajax({
            type: "POST",
            url: "./protected/data.php",
            data: {"type" : "request_freedays", "data" : data},
            cache: false,
            success: function(result)
            {
                alert(result);
            },
            error: function (req, status, error)
            {
                alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
            }
        });
    });
});