$(document).ready(function()
{
    $("#add_feestdag_submit").click(function(e)
    {
        e.preventDefault();

        var data = {"holiday": document.getElementById("holiday").value, "start_date":document.getElementById("add_startdate").value, "end_date":document.getElementById("add_enddate").value};

        $.ajax({
            type: "POST",
            url: "./protected/data.php",
            data: {"type" : "register_bhds", "data" : data},
            cache: false,
            success: function(result)
            {
                alert(result);
            },
            error: function (req, status, error)
            {
                alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
            }
        });
    });
});