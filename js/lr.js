$(document).ready(function()
{   
   $("#register-submit").click(function(e)
   {
       e.preventDefault();
       var email = document.getElementById("register_email").value;
       var username = document.getElementById("register_username").value;
       var password = document.getElementById("register_password").value;
       $.ajax({
            type: "POST",
            url: "protected/register.php",
            data: {"email" : email, "username" : username, "password" : password},
            cache: false,
            success: function(result)
            {
                alert(result);
            },
            error: function (req, status, error)
            {
                alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
            }
        });
       return false;
   });
    
    $("#register_email").blur(function()
    {
        var email = document.getElementById("register_email").value;
        if($(this).val() == '')
        {
            return false;    
        }
        if($(this).val() != '')
        {
            $.ajax({
            type: "POST",
            url: "protected/data.php",
            data: {"type" : "email_exists", "email" : email},
            cache: false,
            success: function(result)
            {
                if(result == 1)
                {
                    $("#register_email").addClass('taken');
                    $("#register_email").removeClass('free');
                }
                else
                {
                    $("#register_email").addClass('free');
                    $("#register_email").removeClass('taken');
                }
            },
            error: function (req, status, error)
            {
                alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
            }
        });
        }
    });
    
    $("#register_username").blur(function()
    {
        var username = document.getElementById("register_username").value;
        if($(this).val() == '')
        {
            return false;    
        }
        if($(this).val() != '')
        {
            $.ajax({
            type: "POST",
            url: "protected/data.php",
            data: {"type" : "username_exists", "username" : username},
            cache: false,
            success: function(result)
            {
                if(result == 1)
                {
                    $("#register_username").addClass('taken');
                    $("#register_username").removeClass('free');
                }
                else
                {
                    $("#register_username").addClass('free');
                    $("#register_username").removeClass('taken');
                }
            },
            error: function (req, status, error)
            {
                alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
            }
        });
        }
    });
    
    $('#login-submit').click(function(e)
    {
        e.preventDefault();
        e.stopPropagation();
        var email = document.getElementById('username').value;
        var password = document.getElementById('password').value;
        $.ajax({
            type: "POST",
            url: "protected/checklogin.php",
            data: {"email" : email, "password" : password},
            cache: false,
            success: function(result)
            {
                alert(result);
                if(result == 1)
                {
                    window.location = "index.php";
                }
                else
                {
                    alert("Username or password wrong...");
                }
            },
            error: function (req, status, error)
            {
                alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
            }
        });
    });
    
    $("#reset-submit").click(function(e)
    {
        e.preventDefault();
        e.stopPropagation();
        if(document.getElementById('username') !== '')
        {
            var email = document.getElementById('username').value;
            $.ajax({
                type: "POST",
                url: "protected/data.php",
                data: {"type" : 'reset_password', "email" : email},
                cache: false,
                success: function(result)
                {
                    if(result == 1)
                    {
                        sweetAlert("Password Reset", "An email with instructions is on it's way to "+email+". It can take up to 10 minutes for the email to arrive and make sure to check your spam forlder! If it's in the spam folder, please mark the sender address as trusted :).");
                    }
                    else
                    {
                        sweetAlert("Password Reset", "Something went wrong, check if you entered the right email address. If you already clicked the reset button within the last 15 minutes and haven't done the reset yet, clicking the button won't do anything good :P");
                    }
                },
                error: function (req, status, error)
                {
                    alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
                }
            });
        }
    });
});

$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});
