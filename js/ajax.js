function Ajax(type, data, url)
{
    if(url == "")
    {
        url = 'protected/data.php';
    }
    $.ajax({
            type: "POST",
            url: data,
            data: {"type" : type, "data" : data},
            dataType: "JSON",
            cache: false,
            success: function(result)
            {
                alert(result);
            },
            error: function (req, status, error)
            {
                alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
            }
        });
}