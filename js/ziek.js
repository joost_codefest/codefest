$(document).ready(function()
{
   $("#submit_ziek").click(function(e)
   {
       e.preventDefault();
       e.stopPropagation();

       var data = {"start_date":document.getElementById("start_date"), "end_date":document.getElementById("end_date"), "request_type":document.getElementById("request_type"), "approve":document.getElementById("approve")};

       $.ajax({
           type: "POST",
           url: data,
           data: {"type" : "callinsick", "data" : data},
           dataType: "JSON",
           cache: false,
           success: function(result)
           {
               alert(result);
           },
           error: function (req, status, error)
           {
               alert("ERROR:" + error.toString() + " " + status + " " + req.responseText);
           }
       });
   });
});