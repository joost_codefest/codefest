<?php
require_once("Includes/DatabaseHandler.php");
require_once("Includes/Security.php");
$dbh = new DatabaseHandler();
$sec = new Security();
if($_POST['password1'] == $_POST['password2'])
{
    if($dbh->SubmitResetValid($_POST['hash']))
    {
        $dbh->UpdatePassword($_POST['hash'], $sec->HashPassword($_POST['password1']));
        header("Location: index.php?rs=true");
    }
    else   
    {
        $dbh->UpdateResetTime($_POST['hash']);
        header("Location: reset.php?hash=".$_POST['hash']."&fail=true");
    }
}
else
{
    if($dbh->SubmitResetValid($_POST['hash']))
    {
        $dbh->UpdateResetTime($_POST['hash']);
        header("Location: reset.php?hash=".$_POST['hash']."&fail=true");
    }
}



?>