<?php
date_default_timezone_set('Europe/Amsterdam');
require_once("./protected/auth/dbh_auth.php");
$dbh = new ADbh();
$c = $dbh->CheckActivationCode($_GET['hash']);
if($c)
{
    $check = $dbh->SetAccountActivated($_GET['hash']);
    if($check)
    {
        echo 'Account successfully activated!';
        echo '
        <script>
        setTimeout(function()
        {
            window.location = "index.php";
        }, 3000);
        </script>
        ';
    }
    else
    {
        echo 'Either this activation code is incorrect, expired or doesn\'t exist at all';
        echo '
        <script>
        setTimeout(function()
        {
            window.location = "index.php";
        }, 3000);
        </script>
        ';
    }
}
else
{
    echo 'Either this activation code is incorrect, expired or doesn\'t exist at all';
    echo '
    <script>
    setTimeout(function()
    {
        window.location = "index.php";
    }, 3000);
    </script>
    ';
}
?>